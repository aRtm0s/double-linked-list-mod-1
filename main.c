#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>  

#include <time.h>

#include "double_linked_list.h"
#include "double2bin_manip.h"

#define LIST_SIZE 100

typedef struct _ThreadData
{
    int number;
    int mode;  // work mode: 1 - from beginning; 0 - from end
    int count; // number of elements counted in thread
    FILE* log; // log-file  
    DoubleLinkedList* list; // list pointer
} ThreadData;

void* threadFunction (void* sharedData);   
void printLogFile (FILE* dest, char str[]); // writes timestamp and message to log-file

int main (int argc, char* argv[])
{
    DoubleLinkedList* myList = createDoubleLinkedList();
    int min, max;

    // Setting the "seed" for generating pseudo random numbers
    srand((unsigned int) time(NULL));

    min = (int) atoi(argv[1]);
    max = (int) atoi(argv[2]);

    printf("First param: %d\n", atoi(argv[1]));
    printf("Second param: %d\n", atoi(argv[2]));
    printf("\nGenerating list with double numbers in [%d; %d]\n", min, max);

    generateDoubleLinkedList(myList, LIST_SIZE, &min, &max);
    printIntDoubleLinkedList(myList, 1);

    // Threads array
    pthread_t* threads = (pthread_t*) malloc(2 * sizeof(pthread_t));

    // Data for threads
    ThreadData* threadData = (ThreadData*) malloc(2 * sizeof(ThreadData));
    
    // Opening/creating log-file for writing
    FILE* fp;
    char name[] = "log_threads.txt";
    if ((fp = fopen(name, "w+")) == NULL) {
        printf("Failed to open file named %s\n", name);
        return 0;
    }

    fprintf(fp ,"\n\t[THREADS LOG]\n\n");

    for (int i = 0; i < 2; i++) {
        if (i == 0) {   // thread #1
            threadData[i].mode = 1;
        } else {
            threadData[i].mode = 0;
        }
        threadData[i].number = i + 1;
        threadData[i].count = 0;
        threadData[i].list = myList;
        threadData[i].log = fp;

        if (pthread_create(&threads[i], NULL, threadFunction, &threadData[i]) != 0) {
		    fprintf(stderr, "error: pthread_create for thread#%d was failed\n", i);
		    exit(-1);
	    }
    }

    // Waiting for all threads
    for (int i = 0; i < 2; i++) {
        pthread_join(threads[i], NULL);
    }

    fprintf(fp ,"\n\t[END THREADS LOG]\n\n");

    printf("Results of processing:\nAmount of\n");
    printf(" - 0's in elements processed by thread#1: %d\n", threadData[0].count);
    printf(" - 1's in elements processed by thread#2: %d\n", threadData[1].count);

    free(threads);
    free(threadData);

    deleteDoubleLinkedList(&myList);

    return 0;
}

void* threadFunction (void* sharedData) {
    ThreadData* thread_data = (ThreadData*) sharedData;
    int counter = 0;
    ListNode* tmp;
    FILE* thread_log = thread_data->log;
    char temp_str[100] = "";
    
    
    sprintf(temp_str, "Thread#%d: starts processing...", thread_data->number);
    printLogFile(thread_log, temp_str);

    if (thread_data->list->size == 0) {
                sprintf(temp_str, "Thread#%d: list is empty, processing ended.", thread_data->number);
                printLogFile(thread_log, temp_str);
                thread_data->count = counter;
                return NULL;
            }

    
    sprintf(temp_str, "Thread#%d: running with mode %d...", thread_data->number, thread_data->mode);
    printLogFile(thread_log, temp_str);

    sprintf(temp_str, "Thread#%d: current list size is %d", thread_data->number, thread_data->list->size);
    printLogFile(thread_log, temp_str);

    if (thread_data->mode) {
        tmp = thread_data->list->head;
        do {
            if (thread_data->list->size == 0) {
                sprintf(temp_str, "Thread#%d: list is empty, processing ended.", thread_data->number);
                printLogFile(thread_log, temp_str);
                thread_data->count = counter;
                pthread_exit(0);
            }

            while( pthread_mutex_trylock(&(tmp->access)) ) {
                sprintf(temp_str, "Thread#%d: waiting for locking mutex...", thread_data->number);
                printLogFile(thread_log, temp_str);
                if (thread_data->list->size == 0) {
                    sprintf(temp_str, "Thread#%d: list is empty, processing ended.", thread_data->number);
                    printLogFile(thread_log, temp_str);
                    thread_data->count = counter;
                    pthread_exit(0);
                } 
                else if (thread_data->list->size == 1 && errno == EBUSY) {
                    sprintf(temp_str, "Thread#%d: last element locked by other thread, processing ended.", 
                            thread_data->number);
                    printLogFile(thread_log, temp_str);
                }
            }

            sprintf(temp_str, "Thread#%d: captured node with data %f",thread_data->number, tmp->value);
            printLogFile(thread_log, temp_str);

            counter += binDigitCounter(&(tmp->value), 0); // Counting 0's
            tmp->status = 1; // mark as processed

            pthread_mutex_unlock(&(tmp->access)); // unlocking access-mutex

            tmp = tmp->next; // going to next node
            popFrontDoubleLinkedList(thread_data->list); // deleting processed node from list
            
            sprintf(temp_str, "Thread#%d: node processed, current list size is %d", 
                thread_data->number, thread_data->list->size);
            printLogFile(thread_log, temp_str);

        } while (1);
    } 
    else 
    {
        tmp = thread_data->list->tail;
        do {
            if (thread_data->list->size == 0) {
                    sprintf(temp_str, "Thread#%d: list is empty, processing ended.", thread_data->number);
                    printLogFile(thread_log, temp_str);

                    thread_data->count = counter;
                    return NULL;
                }

            while( pthread_mutex_trylock(&(tmp->access)) ) {
                sprintf(temp_str, "Thread#%d: waiting for locking mutex...", thread_data->number);
                printLogFile(thread_log, temp_str);

                if (thread_data->list->size == 0) {
                    sprintf(temp_str, "Thread#%d: list is empty, processing ended.", thread_data->number);
                    printLogFile(thread_log, temp_str);

                    thread_data->count = counter;
                    return NULL;
                }
                else if (thread_data->list->size == 1 && errno == EBUSY) {
                    sprintf(temp_str, "Thread#%d: last element locked by other thread, processing ended.", 
                            thread_data->number);
                    printLogFile(thread_log, temp_str);
                }
            }

            sprintf(temp_str, "Thread#%d: captured node with data %f",thread_data->number, tmp->value);
            printLogFile(thread_log, temp_str);

            counter += binDigitCounter(&(tmp->value), 1); // Counting 1's
            tmp->status = 1;

            pthread_mutex_unlock(&(tmp->access));

            tmp = tmp->prev;
            popBackDoubleLinkedList(thread_data->list);

            sprintf(temp_str, "Thread#%d: node processed, current list size is %d", 
                thread_data->number, thread_data->list->size);
            printLogFile(thread_log, temp_str);
            
        } while (1);
    }

    thread_data->count = counter;

    sprintf(temp_str, "Thread #%d: end processing with result %d", 
                thread_data->number, thread_data->count);
    printLogFile(thread_log, temp_str);

    return NULL;
};

void printLogFile (FILE* dest, char str[]) {
    struct tm tm = *localtime(&(time_t){time(NULL)});
    fprintf(dest, "%.2d:%.2d:%.2d %s\n", tm.tm_hour, tm.tm_min, tm.tm_sec, str);
}