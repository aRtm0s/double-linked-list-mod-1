all: main

main:
	gcc main.c double_linked_list.c double2bin_manip.c -Wall -o double_list_check.out -lpthread

clean:
	rm *.out
