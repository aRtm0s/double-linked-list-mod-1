#include "double2bin_manip.h"

int binDigitCounter (double* value, int flag) {
    udouble udb; 
    udb.v = *value;

    __uint64_t mask = 1ULL << 63;   // mask for logical and with bin data
    int counter_0 = 0;  // amount of '0'
    int counter_1 = 0;  // amount of '1'

    if (flag != 0 && flag != 1) {
        exit(2);
    } else {
        do {
            if (mask & udb.bin) {
                counter_1++;
            } else { 
                counter_0++; 
            }

            mask >>= 1;
        } while (mask >= 1);
    }

    // Depends on flag value return the necessary counter
    if (flag) {
        return counter_1;
    } else {
        return counter_0;
    }  
};

void printBinDigit (double* value) {
    udouble udb; 
    udb.v = *value;
    __uint64_t mask = 1ULL << 63;

    do {
        putchar((mask & udb.bin) ? '1' : '0');
        mask >>= 1;
    } while (mask >= 1);

    printf("\n");
};