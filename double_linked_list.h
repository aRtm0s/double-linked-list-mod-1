#ifndef DOUBLE_LINKED_LIST_H
#define DOUBLE_LINKED_LIST_H

#include <stdlib.h>
#include <time.h>
#include <pthread.h>

typedef struct _ListNode {
    double value;
    int status;  // 0 - not processed; 1 - processed
    pthread_mutex_t access;  // mutex for threads control
    struct _ListNode* prev;  // previous item pointer 
    struct _ListNode* next;  // next item pointer
} ListNode;

typedef struct _DoubleLinkedList
{
    int size;     // list size
    ListNode* head;  // first listed emelent
    ListNode* tail;  // last listed element
} DoubleLinkedList;

DoubleLinkedList* createDoubleLinkedList();
void deleteDoubleLinkedList(DoubleLinkedList** list);
void pushFrontDoubleLinkedList(DoubleLinkedList* list, double* data);
void pushBackDoubleLinkedList(DoubleLinkedList* list, double* data);
double popFrontDoubleLinkedList(DoubleLinkedList* list);
double popBackDoubleLinkedList(DoubleLinkedList* list);
void printIntDoubleLinkedList(DoubleLinkedList* list, int row);
void generateDoubleLinkedList(DoubleLinkedList* list, int size, int* min, int* max);

#endif // DOUBLE_LINKED_LIST_H